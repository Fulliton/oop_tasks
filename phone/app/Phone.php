<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
  public function user() {
    return $this->hasOne('App\User', 'id', 'user_id');
  }

  public function type() {
    return $this->hasOne('App\Type', 'id', 'type_id');
  }
}
