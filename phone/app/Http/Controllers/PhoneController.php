<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Phone;
use App\Type;
use App\User;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $phones = Phone::all();
        $types = Type::all();
        $users = User::all();
        return view('phone/index',  compact('phones', 'types', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return dump($request);
        $phone = new Phone;
        $phone->number = $request->number;
        $phone->user_id = $request->user_id;
        $phone->type_id = $request->type_id;
        $phone->save();
        return redirect(route('phone.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone = Phone::where('id', $id)->first();
        $types = Type::all();
        $users = User::all();
        return view('phone/edit',  compact('phone', 'types', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone = Phone::where('id', $id)->first();
        $phone->number = $request->number;
        $phone->user_id = $request->user_id;
        $phone->type_id = $request->type_id;
        $phone->save();
        return redirect(route('phone.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Phone::where('id',$id)->delete();
        return redirect( route('phone.index') );
    }
}
