let canvas = document.getElementById("canvas");
let context = canvas.getContext("2d");

canvas.width = 500;
canvas.height = 500;

let paint = new Paint(canvas, context);
paint.init(paint.figures.LINE, "white", "black", "black")

document.getElementById("line").onclick = function() {
  document.querySelector("a.selected").classList.remove("selected")
  this.classList.add("selected")
  paint.currentFigure = paint.figures.LINE;
}

document.getElementById("square").onclick = function() {
  document.querySelector("a.selected").classList.remove("selected")
  this.classList.add("selected")
  paint.currentFigure = paint.figures.SQUARE;
}

document.getElementById("triangle").onclick = function() {
  document.querySelector("a.selected").classList.remove("selected")
  this.classList.add("selected")
  paint.currentFigure = paint.figures.TRIANGLE
}

document.getElementById("circle").onclick = function() {
  document.querySelector("a.selected").classList.remove("selected")
  this.classList.add("selected")
  paint.currentFigure = paint.figures.CIRCLE
}

function colorChange() {
	paint.changeColors(
		document.getElementById("background").value,
		document.getElementById("stroke").value,
		document.getElementById("fill").value)
}

document.getElementById("background").addEventListener("change", colorChange, false);
document.getElementById("stroke").addEventListener("change", colorChange, false);
document.getElementById("fill").addEventListener("change", colorChange, false);