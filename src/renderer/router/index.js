import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'index',
      component: require('@/components/index').default
    },
    {
      path: '/lab3',
      name: 'lab3',
      component: require('@/components/lab3').default
    },
    {
      path: '/lab4',
      name: 'lab4',
      component: require('@/components/lab4').default
    },
    {
      path: '/lab5',
      name: 'lab5',
      component: require('@/components/lab5').default
    },
    {
      path: '/lab7',
      name: 'lab7',
      component: require('@/components/lab7').default
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
